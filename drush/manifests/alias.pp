define drush::alias (
  $ensure                   = undef,
  $path                     = $drush::config_path,
  $owner                    = 'root',
  $group                    = 'root',
  $uri                      = undef,
  $root                     = undef,
  $site_path                = undef,
  $remote_host              = undef,
  $remote_port              = undef,
  $remote_user              = undef,
  $os                       = undef,
  $ssh_options              = undef,
  $parent                   = undef,
  $databases                = undef,
  $path_aliases             = undef,
  $php                      = undef,
  $php_options              = undef,
  $variables                = undef,
  $command_specific         = undef,
  $source_command_specific  = undef,
  $target_command_specific  = undef
) {
  if ! defined(Class['drush']) {
    fail('The class drush must be defined.')
  }

  if $title =~ /^\w+\.\w+$/ {
    $parts = split($title, '[.]')
    $alias_group = $parts[0]
    $alias_name = $parts[1]
  } else {
    $alias_group = undef
    $alias_name = $title
  }

  if $ensure {
    validate_re($ensure, '^(present|absent)$',
    "${ensure} is not supported for ensure. Allowed values are 'present' and 'absent'")
  }

  validate_absolute_path($path)

  $file_attributes = {
    ensure  => directory,
    owner   => $owner,
    group   => $group,
    mode    => '0755'
  }
  ensure_resource('file', $path, $file_attributes)

  if $root {
    validate_absolute_path($root)
  }

  if $remote_port {
    if ! is_integer($remote_port) {
      fail('Drush alias remote port must be an integer')
    }
  }

  if $php {
    validate_absolute_path($php)
  }

  if $alias_group {
    validate_string($alias_group)
    $group_ensure = getparam(Drush::Alias_group[$alias_group], 'ensure')
  }

  if $group_ensure == 'absent' {
    $alias_ensure = $group_ensure
  } else {
    $alias_ensure = $ensure ? {
      undef   => present,
      default => $ensure
    }
  }

  if $alias_group {
    $real_alias_name = "${alias_group}.${alias_name}"
    $target = "${alias_group}-group"
  } else {
    $real_alias_name = $alias_name
    $target = $alias_name

    concat { $real_alias_name:
      path            => "${path}/${real_alias_name}.alias.drushrc.php",
      owner           => $owner,
      group           => $group,
      ensure          => $alias_ensure,
      ensure_newline  => true
    }

    concat::fragment { "${real_alias_name}-header":
      target  => $target,
      content => template('drush/alias_header.erb'),
      order   => '01'
    }
  }

  concat::fragment { $real_alias_name:
    target  => $target,
    content => template('drush/alias.erb'),
    ensure  => $alias_ensure
  }
}
