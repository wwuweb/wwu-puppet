define apache::vhost (
  $ensure   = present,
  $vhost    = $title,
  $doc_root = undef
) {
  if ! defined(Class['apache']) {
    fail('Class apache must be defined.')
  }

  Exec {
    path => [
      '/usr/local/bin',
      '/usr/local/sbin',
      '/usr/bin',
      '/usr/sbin',
      '/bin',
      '/sbin'
    ]
  }

  exec { "${vhost}-vhost-enable":
    command     => "a2ensite ${vhost}.conf",
    creates     => "${apache::server_root}/${apache::vhosts_enabled}/${vhost}.conf",
    notify      => Service['apache'],
    refreshonly => true
  }

  if $ensure == 'present' {
    file { "${vhost}-doc-root-create":
      path   => "${doc_root}",
      ensure =>  directory
    }

    file { "${vhost}-vhost-conf-create":
      path    => "${apache::server_root}/${apache::vhosts_available}/${vhost}.conf",
      ensure  => present,
      content => template('apache/vhost.erb'),
      mode    => '0644',
      force   => true,
      require => [
        Exec['disable-default-vhosts'],
        File["${vhost}-doc-root-create"]
      ],
      notify  => Exec["${vhost}-vhost-enable"]
    }
  } else {
    exec { "${vhost}-vhost-disable":
      command => "a2dissite ${vhost}.conf",
      notify  => Service['apache']
    }

    file { "${vhost}-vhost-conf-remove":
      path    => "${apache::server_root}/${apache::vhosts_available}/${vhost}.conf",
      ensure  => absent,
      force   => true
    }

    Exec["${vhost}-vhost-disable"] -> File["${vhost}-vhost-conf-remove"]
  }
}
