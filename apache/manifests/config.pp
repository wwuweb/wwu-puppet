define apache::config (
    $file = $title,
    $path = $apache::server_root
) {
  if ! defined(Class['apache']) {
    fail('Class apache must be defined.')
  }

  file { $file:
    path    => "${path}/${file}",
    ensure  => file,
    source  => "puppet:///modules/apache/${file}",
    force   => true,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    notify  => Service['apache'],
    require => Package['apache']
  }
}