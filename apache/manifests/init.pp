class apache (
  $package          = $apache::params::package,
  $service          = $apache::params::service,
  $web_user         = $apache::params::web_user,
  $web_group        = $apache::params::web_group,
  $server_root      = $apache::params::server_root,
  $vhosts_available = $apache::params::vhosts_available,
  $vhosts_enabled   = $apache::params::vhosts_enabled,
  $mods_available   = $apache::params::mods_available,
  $mods_enabled     = $apache::params::mods_enabled
) inherits apache::params {
  Exec {
    path => [
      '/usr/local/bin',
      '/usr/local/sbin',
      '/usr/bin',
      '/usr/sbin',
      '/bin',
      '/sbin'
    ]
  }

  package { $package:
    ensure  => present,
    alias   => 'apache'
  }

  service { $service:
    ensure  => running,
    alias   => 'apache',
    enable  => true,
    require => Package['apache']
  }

  exec { 'disable-default-vhosts':
    cwd     => "${server_root}/${vhosts_enabled}",
    command => 'rm -f *default.conf',
    onlyif  => "ls | grep 'default' >/dev/null 2>/dev/null",
    notify  => Service['apache'],
    require => Package['apache']
  }
}
