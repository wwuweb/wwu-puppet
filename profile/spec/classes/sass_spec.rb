require 'spec_helper'

describe 'profile::sass', :type => :class do

  it 'should compile' do
    compile
  end

  it 'should compile with all dependencies' do
    compile.with_all_deps
  end

  it 'should contain package compass' do
    should contain_package('compass').with(
      :ensure   => '0.12.7',
      :provider => 'gem'
    )
  end

  it 'should contain package sass' do
    should contain_package('sass').with(
      :ensure   => '3.2.19',
      :provider => 'gem'
    )
  end

  it 'should contain package zen-girds' do
    should contain_package('zen-grids').with(
      :ensure   => '1.4',
      :provider => 'gem'
    )
  end

end
