require 'spec_helper'

describe 'profile::webserver::includes', :type => :class do

  it 'should compile' do
    compile
  end

  it 'should compile with all dependencies' do
    compile.with_all_deps
  end

  it 'should contain apache mod include' do
    should contain_apache__module('include')
  end

  it 'should contain includes option' do
    should contain_file_line('serverside_includes_option').with(
      :path => '/etc/apache2/sites-available/drupal.conf',
      :line => 'Options FollowSymLinks Includes',
      :match => 'Options FollowSymLinks',
      :notify => 'Service[apache]'
    )
  end

  it 'should contain includes type' do
    should contain_file_line('serverside_includes_type').with(
      :path => '/etc/apache2/sites-available/drupal.conf',
      :line => 'AddType text/html .shtml',
      :after => 'Require all granted',
      :notify => 'Service[apache]'
    )
  end

  it 'should contain includes filter' do
    should contain_file_line('serverside_includes_filter').with(
      :path => '/etc/apache2/sites-available/drupal.conf',
      :line => 'AddOutputFilter INCLUDES .shtml',
      :after => 'Require all granted',
      :notify => 'Service[apache]'
    )
  end

end
