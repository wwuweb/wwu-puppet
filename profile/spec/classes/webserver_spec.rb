require 'spec_helper'

describe 'profile::webserver', :type => :class do 

  it 'should compile' do
    compile
  end

  it 'should compile with all dependencies' do
    compile.with_all_deps
  end

  it 'should include apache' do
    should contain_class('apache')
  end

  it 'should include mysql' do
    should contain_class('mysql')
  end

  it 'should include php' do
    should contain_class('php')
  end

  it 'should include php::json' do
    should contain_class('php::json')
  end

  it 'should include php::mysql' do
    should contain_class('php::mysql')
  end

  it 'should include apache configuration files' do
    should contain_apache__config('apache2.conf')
    should contain_apache__config('ports.conf')
  end

end
