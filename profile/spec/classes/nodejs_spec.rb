require 'spec_helper'

describe 'profile::nodejs', :type => :class do

  it 'should compile' do
    compile
  end

  it 'should compile with all dependencies' do
    compile.with_all_deps
  end

  it 'should contain package grunt-cli' do
    should contain_package('grunt-cli').with(
      :ensure   => 'present',
      :provider => 'npm'
    )
  end

end
