class profile::webserver::includes inherits profile::webserver {
  $virtual_host = lookup('profile::webserver::drupal::virtual_host')

  apache::module { 'include': }

  file_line { 'serverside_includes_option':
    path   => "${apache::server_root}/${apache::vhosts_available}/${virtual_host}.conf",
    line   => '      Options FollowSymLinks Includes',
    match  => '^\s*Options\ FollowSymLinks\ Includes',
    after  => 'Require all granted',
    notify => Service['apache']
  }

  file_line { 'serverside_includes_type':
    path   => "${apache::server_root}/${apache::vhosts_available}/${virtual_host}.conf",
    line   => '      AddType text/html .shtml',
    match  => '^\s*AddType\ text/html\ \.shtml',
    after  => 'Require all granted',
    notify => Service['apache']
  }

  file_line { 'serverside_includes_filter':
    path   => "${apache::server_root}/${apache::vhosts_available}/${virtual_host}.conf",
    line   => '      AddOutputFilter INCLUDES .shtml',
    match  => '^\s*AddOutputFilter\ INCLUDES\ \.shtml',
    after  => 'Require all granted',
    notify => Service['apache']
  }
}
