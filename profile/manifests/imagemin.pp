class profile::imagemin {
  package { 'build-essential':
    ensure => present
  }

  package { 'pkg-config':
    ensure => present
  }

  package { 'autoconf':
    ensure => present
  }

  package { 'libtool':
    ensure => present
  }

  package { 'nasm':
    ensure => present
  }

  package { 'libpng-dev':
    ensure => present
  }
}
