class profile::python {
  package { 'pylint':
    ensure => present
  }

  package { 'python-dev':
    ensure => present
  }

  package { 'python-pip':
    ensure => present
  }
}
