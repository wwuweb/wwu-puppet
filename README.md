wwu-puppet
==

## Description

A collection of Puppet modules maintained by the Webtech team at WWU. These modules provide the ability to provision various software and components pertaining to web development in a LAMP environment.

### Included Modules

- **apache: Apache web server**
    - Apache service
    - Virtual hosts
    - Modules
- **drupal: Drupal CMS**
    - Download and install Drupal codebase
- **drush: Drush commandline Drupal tool**
    - Aliases
- **git: Git version control**
    - Repositories
    - Branches
    - Remotes
- **mysql: MySQL database**
    - MySQL service
    - Databses
    - Users
- **php: PHP server-side scripting language**
    - curl
    - gd
    - json
    - ldap
    - mcrypt
    - mysql
    - xdebug
- **ssh: SSH client**
    - Known hosts
- **guest_additions: Virtualbox guest additions**
- **zip: Zip/Unzip commandline tools**

## Setup

### Requirements

This suite depends on the following Puppet modules:

- [**puppetlabs-stdlib**](https://github.com/puppetlabs/puppetlabs-stdlib/)

Specific modules, such as `drush` and `ssh` further depend on:

- [**puppetlabs-concat**](https://github.com/puppetlabs/puppetlabs-concat/)
- [**npacker-composer**](https://github.com/npacker/npacker-composer/)

These requirements can be easily provisioned by running:

```
puppet module install puppetlabs-stdlib
puppet module install puppetlabs-concat
```

## Limitations

This suite supports Puppet in versions **>= 2.7, <3.5**

It has been tested on the following platforms:

- Ubuntu 14.04 LTS

## Development

The modules in this suite implement `rspec-puppet` for unit testing. Functional testing is currently applied manually in a Vagrant VM to realize application in a simulated server scenario.

### Unit Tests

New fixes or features should be accompanied by RSpec tests to verify the integrity of the changes in the overtall codebase. Ideally, the full suite should be checked for a passing status before opening a pull request. The gems necessary for running the test suite are cataloged in the included Gemfile. The `bunder` gem must be available in the development environment to install `rspec-puppet` and it's dependencies:

```
gem install bundler
```

To initialize the required testing gems, run the following command from the project root:

```
bundle install
```

To execute the test suite, navigate the the root directory of a module and run:

```
bundle exec rake spec
```

### Syntax and Lint

To verify the integrity of the code and formatting, run the rake tasks:

```
bundle exec rake syntax
```

And:

```
bundle exec rake lint
```

These tasks should pass without errors before opening a pull request.

### Functional Tests

Currently, this suite relies on the manual application of functional tests, which are included in the `test` directories in each module. It is recommended to apply these tests with the --debug and --noop flags. Applying tests in a Vagrant VM allows the environment to be restored after each test run via `vagrant destroy`. In future, it is planned to use `beaker` to automate this process.
