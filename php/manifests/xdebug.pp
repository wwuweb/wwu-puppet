class php::xdebug (
  $package     = $php::params::xdebug_package,
  $config_root = $php::params::config_root
) inherits php::params {
  package { $package:
    ensure  => present,
    alias   => 'php-xdebug',
    require => Package['php']
  }

  $path = "${config_root}/mods-available/xdebug.ini"

  file_line { 'xdebug-remote-enable':
    path    => $path,
    line    => 'xdebug.remote_enable=1',
    notify  => Service['apache'],
    require => Package['php-xdebug']
  }

  file_line { 'xdebug-extended-info':
    path    => $path,
    line    => 'xdebug.extended_info=1',
    notify  => Service['apache'],
    require => Package['php-xdebug']
  }

  file_line { 'xdebug-remote-port':
    path    => $path,
    line    => 'xdebug.remote_port=9000',
    notify  => Service['apache'],
    require => Package['php-xdebug']
  }

  file_line { 'xdebug-remote-connect-back':
    path    => $path,
    line    => 'xdebug.remote_connect_back=1',
    notify  => Service['apache'],
    require => Package['php-xdebug']
  }

  file_line { 'xdebug-var-display-max-depth':
    path    => $path,
    line    => 'xdebug.var_display_max_depth=5',
    notify  => Service['apache'],
    require => Package['php-xdebug']
  }

  file_line { 'xdebug-max-nesting-level':
     path    => $path,
     line    => 'xdebug.max_nesting_level=256',
     notify  => Service['apache'],
     require => Package['php-xdebug']
   }
}
