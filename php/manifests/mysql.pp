class php::mysql (
  $package = $php::params::mysql_package
) inherits php::params {
  package { $package:
    ensure  => present,
    require => Package['php']
  }
}