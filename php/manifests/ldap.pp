class php::ldap (
  $package = $php::params::ldap_package
) inherits php::params {
  package { $package:
    ensure  => present,
    require => Package['php']
  }
}