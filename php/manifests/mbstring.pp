class php::mbstring (
  $package = $php::params::mbstring_package
) inherits php::params {
  package { $package:
    ensure  => present,
    require => Package['php']
  }
}
