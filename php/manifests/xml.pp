class php::xml (
  $package = $php::params::xml_package
) inherits php::params {
  package { $package:
    ensure  => present,
    require => Package['php']
  }
}
