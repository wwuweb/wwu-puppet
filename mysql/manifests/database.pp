define mysql::database (
  $ensure = present,
  $database = $title
) {
  if ! defined(Class['mysql']) {
    fail('Class mysql must be defined.')
  }

  if $ensure == 'present' {
    mysql::query { "${database}-database-present":
      query => template('mysql/create-database.erb')
    }
  } elsif $ensure == 'absent' {
    mysql::query { "${database}-database-absent":
      query => template('mysql/drop-database.erb')
    }
  }
}