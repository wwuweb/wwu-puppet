define mysql::query (
  $query    = undef,
  $database = undef,
  $onlyif   = undef,
  $unless   = undef
) {
  if ! defined(Class['mysql']) {
    fail('Class mysql must be defined.')
  }

  Exec {
    environment => "HOME=/home/${mysql::runas}",
    user        => $mysql::runas,
    path        => [
      '/usr/local/bin',
      '/usr/local/sbin',
      '/usr/bin',
      '/usr/sbin',
      '/bin',
      '/sbin'
    ],
    require     => File['create-client-my-cnf']
  }

  $escapedquery = regsubst($query, '\"', '\\\"')
  $command      = template('mysql/query.erb')

  exec { "${query}":
    command => $command,
    onlyif  => $onlyif,
    unless  => $unless
  }
}
