define ssh::known_host (
  $ensure = present,
  $host   = $title
) {
  if ! defined(Class['ssh']) {
    fail('The class ssh must be defined.')
  }

  validate_re($ensure, '^(present|absent)$',
  "${ensure} is not supported for ensure. Allowed values are 'present' or 'absent'")

  Exec {
    path => [
      '/usr/local/bin',
      '/usr/local/sbin',
      '/usr/bin',
      '/usr/sbin',
      '/bin',
      '/sbin'
    ]
  }

  exec { "get-host-key-${host}":
    command => "ssh-keyscan -t rsa ${host} >> ${::ssh_basedir}/ssh-key-${host}",
    unless  => "test -e ${::ssh_basedir}/ssh-key-${host}"
  }

  concat::fragment { "known-host-${host}":
    target  => 'ssh_known_hosts',
    source  => "${::ssh_basedir}/ssh-key-${host}",
    ensure  => $ensure
  }

  Exec["get-host-key-${host}"] -> Concat::Fragment["known-host-${host}"]
}
