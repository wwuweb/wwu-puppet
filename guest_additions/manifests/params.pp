class guest_additions::params {
  $cwd          = '/home/vagrant'
  $mount_point  = '/tmp/VBoxGuestAdditions'
  $install_dir  = '/opt'
  $link_target  = '/usr/lib'
}
