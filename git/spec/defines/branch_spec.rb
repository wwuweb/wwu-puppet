require 'spec_helper'

describe 'git::branch', :type => :define do

  let(:facts) { {:concat_basedir => '/foo'} }

  let(:pre_condition) { 
    'include ssh
     include git
     git::repository { "wwuweb/collegesites": host => "bitbucket.org", protocol => "ssh", directory => "/vagrant/collegesites", owner => "vagrant" }'
  }

  let(:title) { 'wwuweb/collegesites:foo' } 

  it "should compile" do
    compile
  end

  it "should compile with all dependencies" do
    compile.with_all_deps
  end

  it {
    is_expected.to contain_exec('git-checkout-wwuweb/collegesites:foo').with(
      :user    => 'vagrant',
      :command => 'git checkout -B foo',
      :unless  => "git branch | grep '* foo' >/dev/null 2>/dev/null"
    ).that_requires('Exec[git-fetch-wwuweb/collegesites]')
  }

  it {
    is_expected.not_to contain_exec('git-detete-branch-wwuweb/collegesites:foo')
  }

  context 'when :ensure => :absent' do
    let(:params) { {:ensure => 'absent'} }

    it {
      is_expected.to contain_exec('git-detete-branch-wwuweb/collegesites:foo').with(
        :user    => 'vagrant',
        :command => 'git branch -D foo',
        :onlyif  => "git branch | grep 'foo' >/dev/null 2>/dev/null"
      ).that_requires('Exec[git-fetch-wwuweb/collegesites]')
    }

    it {
      is_expected.not_to contain_exec('git-checkout-wwuweb/collegesites:foo')
    }
  end

end
