require 'spec_helper'

describe 'git::remote', :type => :define do

  let(:facts) { {:concat_basedir => '/foo'} }

  let(:pre_condition) { 
    'include ssh
     include git
     git::repository { "wwuweb/collegesites": host => "bitbucket.org", protocol => "ssh", directory => "/vagrant/collegesites", owner => "vagrant" }'
  }

  let(:title) { 'wwuweb/collegesites:foo' } 

  let(:params) {
    {
      :host     => 'bitbucket.org',
      :protocol => 'https'
    }
  }

  it "should compile" do
    compile
  end

  it "should compile with all dependenceis" do
    compile.with_all_deps
  end

  it {
    is_expected.to contain_exec('git-remote-add-wwuweb/collegesites:foo').with(
      :user    => 'vagrant',
      :command => 'git remote add foo https://bitbucket.org/wwuweb/collegesites.git',
      :unless  => "git remote --verbose | grep 'foo' >/dev/null 2>/dev/null"
    ).that_requires('Exec[git-init-wwuweb/collegesites]').that_requires('Exec[git-clone-wwuweb/collegesites]')
  }

  it {
    is_expected.to contain_exec('git-remote-update-wwuweb/collegesites:foo').with(
      :user    => 'vagrant',
      :command => 'git remote set-url foo https://bitbucket.org/wwuweb/collegesites.git',
      :unless  => "git remote --verbose | grep 'https://bitbucket.org/wwuweb/collegesites.git' >/dev/null 2>/dev/null"
    ).that_requires('Exec[git-remote-add-wwuweb/collegesites:foo]')
  }

end
