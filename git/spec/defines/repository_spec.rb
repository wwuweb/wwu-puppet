require 'spec_helper'

describe 'git::repository', :type => :define do

  let(:pre_condition) do
    'include ssh include git'
  end 

  let(:facts) { { :concat_basedir => '/foo' } }

  let(:title) { 'wwuweb/collegesites' }

  let(:params) {
    {
      :host      => 'bitbucket.org',
      :directory => '/vagrant/collegesites'
    }
  }

  it "should compile" do
    compile
 end

  it "should compile with all dependencies" do
    compile.with_all_deps
  end

  context 'when protocol is https' do
    let(:params) {
      { 
        :host       => 'bitbucket.org',
        :protocol   => 'https',
        :directory => '/vagrant/collegesites'
      }
    }

    it {
      is_expected.to contain_exec('git-clone-wwuweb/collegesites').with(
        :command  => 'git clone https://bitbucket.org/wwuweb/collegesites.git /vagrant/collegesites',
        :unless   => 'test -d /vagrant/collegesites',
        :user     => 'root',
        :tries    => 2
      ).that_notifies('Exec[git-fetch-wwuweb/collegesites]')
    }

    it {
      is_expected.to contain_git__remote('wwuweb/collegesites:origin').with(
        :host     => 'bitbucket.org',
        :protocol => 'https'
      )
    }
  end

  context 'when protocol is ssh' do
    let(:params) {
      {
        :host      => 'bitbucket.org',
        :protocol  => 'ssh',
        :directory => '/vagrant/collegesites'
      }
    }

    it {
      is_expected.to contain_exec('git-clone-wwuweb/collegesites').with(
        :command  => 'git clone git@bitbucket.org:wwuweb/collegesites.git /vagrant/collegesites',
        :unless   => 'test -d /vagrant/collegesites',
        :user     => 'root',
        :tries    => 2
      ).that_notifies('Exec[git-fetch-wwuweb/collegesites]')
    }

    it {
      is_expected.to contain_git__remote('wwuweb/collegesites:origin').with(
        :host     => 'bitbucket.org',
        :protocol => 'ssh'
      )
    }
  end

  it {
    is_expected.to contain_exec('git-fetch-wwuweb/collegesites').with(
      :cwd          => '/vagrant/collegesites',
      :command      => 'git fetch',
      :refreshonly  => true
    ).that_requires('Git::Remote[wwuweb/collegesites:origin]')
  }

end
