include git

git::repository { 'wwuweb/collegesites':
  host      => 'bitbucket.org',
  protocol  => 'https',
  directory => '/vagrant/collegesites'
}