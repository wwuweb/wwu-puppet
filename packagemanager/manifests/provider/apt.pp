class packagemanager::provider::apt {
  if ! defined(Class['packagemanager']) {
    fail('Class packagemanager must be defined.')
  }

  Exec {
    path => [
      '/usr/local/bin',
      '/usr/local/sbin',
      '/usr/bin',
      '/usr/sbin',
      '/bin',
      '/sbin'
    ]
  }

  file { 'dpkg-updates':
    path => '/var/lib/dpkg/updates',
    ensure       => directory,
    purge        => true,
    recurse      => true,
    recurselimit => 1,
    force        => true
  }

  file { 'apt-lists':
    path         => '/var/lib/apt/lists',
    ensure       => directory,
    purge        => true,
    recurse      => true,
    recurselimit => 1,
    force        => true
  }

  exec { 'update-apt-package-lists':
    command   => 'apt-get --yes --fix-missing --quiet update',
    timeout   => 300,
    tries     => 3,
    logoutput => on_failure,
    require   => [ File['dpkg-updates'], File['apt-lists'] ],
    notify    => [ Exec['autoremove-apt-packages'], Exec['autoclean-apt-packages'] ]
  }

  exec { 'autoremove-apt-packages':
    command     => 'apt-get --yes --quiet autoremove',
    logoutput   => on_failure,
    refreshonly => true
  }

  exec { 'autoclean-apt-packages':
    command     => 'apt-get --yes --quiet autoclean',
    logoutput   => on_failure,
    refreshonly => true
  }
}
